const Albums = require('./Albums');

class App {
	constructor() {
		this.albums = new Albums();
	}

	start() {
		const stdin = process.openStdin();
		const {albums} = this;

		console.log('Welcome to your music collection! Enter --help for instructions. \n\n');

		stdin.addListener("data", d => {
			let text = d.toString();

			if(text.startsWith('--help')) {
				this.showInstructions();
			} else if(text.startsWith('add')) {
				albums.addAlbum(text);
			} else if(text.startsWith('play')) {
				albums.play(text);
			} else if(text.startsWith('show unplayed by')) {
				albums.showAllUnplayedByArtist(text);
			} else if(text.startsWith('show unplayed')) {
				albums.showUnplayed();
			} else if(text.startsWith('show all by')) {
				albums.showAllByArtist(text);
			} else if(text.startsWith('show all')){
				albums.showAll();
			} else if(text.startsWith('quit')) {
				this.end();
			} else {
				console.log('Invalid command.');
			}

			console.log('\n');
		});
	}

	showInstructions() {
		let string =
		`add "$title" "$artist": adds an album to the collection with the given title and artist. All albums are unplayed by default.\r\n\r\nplay "$title": marks a given album as played.\r\n\r\nshow all: displays all of the albums in the collection.\r\n\r\nshow unplayed: display all of the albums that are unplayed.\r\n\r\nshow all by "$artist": shows all of the albums in the collection by the given artist.\r\n\r\nshow unplayed by "$artist": shows the unplayed albums in the collection by the given artist\r\n\r\nquit: quits the program
		`;

		console.log(string);
	}

	end() {
		process.exit();
	}
}

const app = new App();
app.start();