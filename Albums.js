const utils = require("./Utils");
const Album = require("./Album");

class Albums {
	constructor() {
		this.albums = [];

		this.addAlbum = this.addAlbum.bind(this);
		this.showAll = this.showAll.bind(this);
		this.play = this.play.bind(this);
		this.showUnplayed = this.showUnplayed.bind(this);
	}

	getAll() {
		if(!this.albums.length) {
			console.log('No albums exist.');
			return false;
		}

		return this.albums;
	}

	addAlbum(text) {
		let inputs = utils.string.quotesToArray(text);
		let title = inputs[0];
		let artist = inputs[1];

		let songDoesNotExist = this.albums.findIndex(album => album.title == title) === -1;

		if(songDoesNotExist) {
			this.albums.push(
				new Album({ title, artist })
			);
			console.log(`Added ${title} by ${artist}`);
			return true;
		} else {
			console.log('Song already exists in the albums.');
			return false;
		}
	}

	showAll() {
		if(!this.albums.length) {
			console.log('No albums exist.');
			return false;
		}

		let played = false;

		this.albums.forEach(album => {
			played = album.played ? "(played)" : "(unplayed)";
			console.log(`${album.title} by ${album.artist} ${played}`);
		});

		return true;
	}

	play(text) {
		let inputs = utils.string.quotesToArray(text);
		let title = inputs[0];

		if(!title) {
			console.log('Please enter a valid song name.');
			return false;
		}


		let songIdx = this.albums.findIndex(album => album.title == title);

		if(songIdx === -1) {
			console.log('This song isn\'t in your playlist.');
			return false;
		} else if(this.albums[songIdx].played) {
			console.log(`You're aleady listening to "${this.albums[songIdx].title}."`);
		} else {
			this.albums[songIdx].played = true;
			console.log(`You're listening to "${this.albums[songIdx].title}"`);
			return true;
		}
	}

	showUnplayed() {
		if(!this.albums.length) {
			console.log('No albums exist.');
			return false;
		}

		let noUnplayedSongs = this.albums.findIndex(album => album.played == false) === -1;

		if(noUnplayedSongs) {
			console.log(`No unplayed songs in albums.`);
			return false;
		} else {
			this.albums.forEach(album => {
				if(!album.played) {
					console.log(`"${album.title}" by ${album.artist}`);
				}
			});

			return true;
		}
	}

	showAllByArtist(text) {
		let inputs = utils.string.quotesToArray(text);
		let artist = inputs[0];
		let played = false;

		if(!artist) {
			console.log('You did not specify an artist name.');
			return false;
		}

		let noSongsByArtist = this.albums.findIndex(song => song.artist == artist) === -1;

		if(noSongsByArtist) {
			console.log(`No songs by artist ${artist} in albums.`);
			return false;
		} else {
			this.albums.forEach(album => {
				if(album.artist == artist) {
					played = album.played ? "(played)" : "(unplayed)";
					console.log(`${album.title} by ${album.artist} ${played}`);
				}
			})
			return true;
		}
	}

	showAllUnplayedByArtist(text) {
		let inputs = utils.string.quotesToArray(text);
		let artist = inputs[0];
		let played = false;

		if(!artist) {
			console.log('You did not specify an artist name.');
			return false;
		}

		let noUnplayedSongs = this.albums.findIndex(album => {return album.artist == artist && album.played === false}) === -1;

		if(noUnplayedSongs) {
			console.log(`No unplayed songs by artist ${artist}.`);
			return false;
		} else {
			this.albums.forEach(album => {
				if(album.artist == artist && album.played === false) {
					console.log(`"${album.title}" by ${album.artist}"`);
				}
			})
			return true;
		}
	}
}

module.exports = Albums;