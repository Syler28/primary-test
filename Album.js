class Album {
	constructor(args) {
		let {title, artist} = args;

		if(!title || !artist) {
			console.log('Missing title or Artist.');
			return false;
		}

		return {
			title,
			artist,
			played: false
		};
	}
}

module.exports = Album;